provider "aws" {
  region = var.region
}

variable "suffix" {
  default = ""
}

variable "region" {
  default = "us-east-1"
}

locals {
  app_name = var.suffix != ""? "someapp-${var.suffix}": "someapp"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "this-is-a-bucket-for-${local.app_name}-${var.region}"
}

terraform {
  backend "s3" {
    bucket               = "this-bucket-is-for-state"
    dynamodb_table       = "tf-state-lock"
    workspace_key_prefix = "default"
    key                  = "terraform.tfstate"
    region               = "us-east-1"
  }
}
